package com.projuris.service;

import com.projuris.entity.User;
import com.projuris.repository.UserRepository;

public class UserService {
	
	private final  UserRepository userRepository = new UserRepository();

	public void save(User user) {
		try {
			userRepository.persist(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
