package com.projuris.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/first-test")
public class FirstTest {

	@GET
	public int getValue() {
		
		Integer r = 0;
		
		Integer[] arrayOne = {0,1,3,4};
		Integer[] arrayTwo = {3,4,10,12};
	
		
		for (int i = 0; i < arrayTwo.length; i++) {
			Integer integer = arrayTwo[i];
			
			Integer q = java.util.Arrays.asList(arrayOne).indexOf(integer);
			
			if(r != -1) {
				r = q;
			}
		}
		
		return 0;
	}
	
}
