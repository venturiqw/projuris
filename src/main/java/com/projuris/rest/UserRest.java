package com.projuris.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.projuris.entity.User;
import com.projuris.repository.UserRepository;
import com.projuris.service.UserService;

@Path("/user")
public class UserRest {
	
	private UserService userService = new UserService();
	
	private final  UserRepository userRepository = new UserRepository();
	
	private Gson gson = new Gson();
	
	@POST
	@Consumes("application/json; charset=UTF-8")
	@Produces("application/json; charset=UTF-8")
	public Response get(String jsonUser) {
		User user = this.gson.fromJson(jsonUser, User.class);
		userService.save(user);
		return Response.ok(user).build();
	}
	
	@GET
	@Produces("application/json; charset=UTF-8")
	public List<User> getAll() {
		List<User> users = this.userRepository.findAll();
		return users;
	}

}
