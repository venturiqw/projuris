package com.projuris.repository;

import java.util.List;

import com.projuris.entity.User;

public class UserRepository extends GenericRepository {
	
	public void persist(User user) {
		this.getEntityManager().getTransaction().begin();
		this.getEntityManager().persist(user);
		this.getEntityManager().getTransaction().commit();
	}

	@SuppressWarnings("unchecked")
	public List<User> findAll() {
		return this.getEntityManager().createQuery("SELECT u FROM User u ORDER BY u.name").getResultList();
	}
	

}
