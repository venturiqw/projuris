package com.projuris.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class GenericRepository {
	
	private final EntityManagerFactory entityManagerFactory;
	private final EntityManager entityManager;
	
	public GenericRepository() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistence_unit_projuris");;
		this.entityManager = this.entityManagerFactory.createEntityManager();
	}
	
	public EntityManager getEntityManager() {
        return this.entityManager;
    }
	
}
